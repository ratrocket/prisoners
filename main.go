package main

import (
	"flag"
	"fmt"
	"io"
	"math/rand"
	"os"
	"time"
)

const VERSION = "1.0.0"

func done(prisoners []bool) bool {
	for _, p := range prisoners {
		if p == false {
			return false
		}
	}
	return true
}

func info(w io.Writer, msg string, pos, sentinelPos, nvisits int) {
	return
	infoFmt := "%s\t(I am %d / sent: %d) [i:%d]\n"
	fmt.Fprintf(w, infoFmt, msg, pos, sentinelPos, nvisits)
}

func summary(list []bool) string {
	m := map[bool]string{
		true:  "x",
		false: "-",
	}
	s := ""
	for _, l := range list {
		s = s + m[l]
	}
	return s
}

func main() {
	var (
		n = flag.Int("n", -1, "number of prisoners (>1)")
		d = flag.Int("d", 250, "delay in milliseconds")
		s = flag.Bool("s", true, "print summary")
		v = flag.Bool("v", false, "print version")
	)
	flag.Parse()
	if *v {
		fmt.Println(VERSION)
		return
	}
	if *n < 2 {
		flag.Usage()
		return
	}

	rand.Seed(time.Now().UnixNano())

	var (
		np          int = *n // total number of prisoners
		nvisits     int      // total number of visits
		sentinelPos int      // position of sentinel in array of prisoners
		lightOn     bool
		delay       time.Duration = time.Duration(*d)
		prefix      string
	)

	ps := make([]bool, np) // init to all false, thanks go
	sentinelPos = rand.Intn(np)

	for {
		curr := rand.Intn(np)
		sentinel := curr == sentinelPos
		nvisits += 1
		if sentinel {
			if lightOn {
				info(os.Stdout, "OFF", curr, sentinelPos, nvisits)
				prefix = "sentinel "
				ps[curr] = true
				lightOn = false
			} else {
				prefix = "waste    "
			}
		} else if !lightOn && !sentinel && !ps[curr] {
			info(os.Stdout, "ON", curr, sentinelPos, nvisits)
			prefix = "turn ON  "
			ps[curr] = true
			lightOn = true
		} else {
			prefix = "         "
		}
		if *s {
			fmt.Printf("%s%s day %d\n", prefix, summary(ps), nvisits)
			time.Sleep(delay * time.Millisecond)
		}
		if sentinel && done(ps) {
			break
		}
	}
	fmt.Printf("Total visits: %d (%.2f years)\n", nvisits, float64(nvisits)/365)
}

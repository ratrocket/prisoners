# prisoners (puzzle)

> **(May 2021)** moved to [md0.org/prisoners](https://md0.org/prisoners).

A little program illustrating a solution of the puzzle that goes:

> There are 100 prisoners in separate cells.  The only way they can
> communicate is that every day a randomly selected prisoner is let into
> a room with a light and a lightswitch, and that prisoner can either
> turn on or turn off the light.
>
> The randomly selected prisoner can also say to the guard "all 100
> prisoners have visited the lightswitch room".  If the prisoner is
> correct, all the prisoners are released.  If the prisoner is not
> correct, all the prisoners are killed.  So you have to be certain when
> making that statement!
>
> The prisoners are all imprisoned at the same time and are given some
> amount of time (15 or 30 minutes, or an evening) to discuss their
> strategy.  After that, they cannot talk to each other and never see
> each other.  They can't see the light from their cells.
>
> The light is initially turned off.

How do they survive and get released??!?  Don't read the code if you
don't want to know a solution!

(See [this
site](https://www.cut-the-knot.org/Probability/LightBulbs.shtml) for an
explanation that might have more details, but also hints and solutions,
so BE CAREFUL!)

## usage

```
  -d int
    	delay in milliseconds (default 250)
  -n int
    	number of prisoners (>1) (default -1)
  -s	print summary (default true)
```

For not printing the summary, do `-s=false` or `-s=0`.  This won't work:
`-s false`.

If `-s=false` or equivalent is given, the output delay (`-d`) doesn't
affect anything.

## pro tip

If you save the output to a file (say, output.txt), you can "replay"
the session and see how it unfolds using my "delayed cat" tool,
[slowcat](https://git.sr.ht/~md0/slowcat/).  `slowcat -d 150 <
output.txt`.  It's pretty fun to watch a 100 prisoner game play out...

## have fun!
